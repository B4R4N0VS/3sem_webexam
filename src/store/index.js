import {createStore} from 'vuex'
import tours from './modules/tours'

const store = createStore({

    modules: {
        tours
    }

})

export default store